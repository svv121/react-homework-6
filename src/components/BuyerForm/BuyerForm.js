import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import TextError from './TextError';
import FormSchema from './FormSchema';
import './BuyerForm.scss';
import {useDispatch} from "react-redux";
import {clearCartLsAC} from "../../store/products/actionCreatorsProducts";

const BuyerForm = (props) => {
    const dispatch = useDispatch();
    const {cartArr, productsInCart, setCartArr, incrementCartCount} = props;

    const initialValues = {
        firstName: '',
        lastName: '',
        email: '',
        age: '',
        phoneNumber: '',
        address: ''
    }

    const handleSubmit = (values, submitProps) => {
        console.log('Products purchased by the buyer: ', productsInCart)
        console.log('Data entered by the buyer: ', values);
        //console.log('submitProps', submitProps);
        submitProps.setSubmitting(false);
        submitProps.resetForm();
        incrementCartCount(-cartArr.length);
        setCartArr([]);
        dispatch(clearCartLsAC());
    }

    if (productsInCart.length > 0) {
    return (
    <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={FormSchema}
    >
        {formik => {
            //console.log('Formik props', formik)
            return (
                <Form className='form-container'>
                    <div className='form-introduction'>Please fill out the form below to complete your purchase</div>
                    <div className='form-control'>
                        <label htmlFor='firstName'>First Name</label>
                        <Field placeholder='First Name' type='text' id='firstName' name='firstName'/>
                        <ErrorMessage name='firstName' component={TextError}/>
                    </div>

                    <div className='form-control'>
                        <label htmlFor='lastName'>Last Name</label>
                        <Field placeholder='Last Name' type='text' id='lastName' name='lastName'/>
                        <ErrorMessage name='lastName' component={TextError}/>
                    </div>

                    <div className='form-control'>
                        <label htmlFor='email'>Email</label>
                        <Field type='email' id='email' name='email'/>
                        <ErrorMessage name='email' component={TextError}/>
                    </div>

                    <div className='form-control'>
                        <label htmlFor='age'>Age</label>
                        <Field placeholder='Age' type='number' id='age' name='age'/>
                        <ErrorMessage name='age' component={TextError}/>
                    </div>

                    <div className='form-control'>
                        <label htmlFor='phoneNumber'>Phone Number</label>
                        <Field placeholder='Phone Number' type='text' id='phoneNumber' name='phoneNumber'/>
                        <ErrorMessage name='phoneNumber' component={TextError}/>
                    </div>

                    <div className='form-control'>
                        <label htmlFor='address'>Address</label>
                        <Field as='textarea' placeholder='Address' id='address' name='address'/>
                        <ErrorMessage name='address' component={TextError}/>
                    </div>

                    <button
                        type='submit'
                        disabled={!formik.isValid || formik.isSubmitting || !formik.dirty}
                        className='checkout-btn'
                    >
                        Checkout
                    </button>
                </Form>
            )
        }}
    </Formik>
    )
    }
    else{
        return <p className='empty-cart'>is empty</p>
    }
}

export default BuyerForm