import './App.css';
import React, { useState, useEffect } from 'react';
import Header from "./components/Header/Header";
import Routing from "./routing/Routing";
import PropTypes from "prop-types";
import {getProductsAC} from "./store/products/actionCreatorsProducts";
import {useDispatch} from 'react-redux'

function App() {
    const [cartCount, setCartCount] = useState(0);
    const [favoriteCount, setFavoriteCount] = useState(0);
    const dispatch = useDispatch();

    const incrementCartCount = (num) => {
        localStorage.setItem('cartCount', JSON.stringify(cartCount + num));
        return setCartCount(cartCount + num)
    }

    const incrementFavoriteCount = (num) => {
        localStorage.setItem('favoriteCount', JSON.stringify(favoriteCount + num));
        return setFavoriteCount(favoriteCount + num)
    }

    useEffect(() => {
        dispatch(getProductsAC());
        if (localStorage.getItem('cartArr')) {
            setCartCount(+JSON.parse(localStorage.getItem('cartArr')).length)
        } else {setCartCount(0)}
        if (localStorage.getItem('favoriteArr')) {
            setFavoriteCount(+JSON.parse(localStorage.getItem('favoriteArr')).length)
        } else {setFavoriteCount(0)}
    }, [dispatch]);

    return (
        <div className="App">
            <Header
                favoriteCount={favoriteCount}
                cartCount={cartCount}
            />
            <Routing
                incrementCartCount={incrementCartCount}
                incrementFavoriteCount={incrementFavoriteCount}
            />
        </div>
    );
}

App.propTypes = {
    productsData: PropTypes.array
};

App.defaultProps = {
    data: []
};

export default App;