import reducerProducts from "./reducerProducts";
import { GET_PRODUCTS } from "./actionsProducts";

const initialState = {products: []}

describe('Products reducer working', () => {

    test('Should return the initial state', () => {
        expect(reducerProducts(undefined, { type: undefined })).toEqual(initialState)
    })

    test('Should change state', () => {
        expect(reducerProducts(initialState, { type: GET_PRODUCTS, payload: [1, 2, 3] })).toEqual({
            products: [1, 2, 3]
        })
    })
})