import React from 'react';
import ReactDOM from 'react-dom/client';
import './reset.css';
import './index.css';
import App from './App';
import { BrowserRouter } from "react-router-dom";
import { Provider } from 'react-redux';
import store from './store'
import ViewContextProvider from "./contexts/ViewContext/ViewContextProvider";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <Provider store={store}>
            <BrowserRouter>
                <ViewContextProvider>
                <App />
                </ViewContextProvider>
            </BrowserRouter>
        </Provider>
    </React.StrictMode>
)