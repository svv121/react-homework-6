import React from 'react';
import styles from "./Product.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import { ReactComponent as StarEmpty } from "../../assets/svg/star-empty.svg";
import { ReactComponent as StarFilled } from "../../assets/svg/star-filled.svg";
import {useDispatch} from "react-redux";
import openModalAC from '../../store/modal/actionCreatorsModal'

const Product = (props) => {
     const {
      productInfo,
      currency,
      isFavorite,
      changeFavoriteArr,
      setCurrentProduct,
      incrementFavoriteCount
       } = props;
     const {id, name, price, url, article, color} = productInfo;

    const dispatch = useDispatch();

    return (
      <div className={styles.case}>
        <div onClick={() => changeFavoriteArr(id)}>
          {isFavorite?
              <StarFilled onClick={() => incrementFavoriteCount(-1)} className={styles.star}/> :
              <StarEmpty onClick={() => incrementFavoriteCount(1)} className={styles.star}/>
          }
        </div>
        <div className={styles.productImgWrapper}>
          <img src={url} alt={name} className={styles.productImg} />
        </div>
        <h2 className={styles.name}>{name}</h2>
        <div className={styles.priceWrapper}>
            <h4 className={styles.price}>Price: {currency}{price}</h4>
            <Button handleClick={() => {
                    setCurrentProduct(productInfo);
                    dispatch(openModalAC(true))}}
                    backgroundColor="#64B743"
                    text="Add to cart"
                    id={id}
            />
        </div>
        <p>Article: {article}</p>
        <p>Color: {color}</p>
      </div>
    );
}

Product.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  url: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  currency: PropTypes.string.isRequired,
  article: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
  isFavorite: PropTypes.bool,
  changeFavoriteArr: PropTypes.func,
  incrementCartCount: PropTypes.func,
  incrementFavoriteCount: PropTypes.func,
};

Product.defaultProps = {
  isFavorite: false,
  changeFavoriteArr:  () => {},
  incrementCartCount: () => {},
  incrementFavoriteCount:  () => {}
};

export default Product;