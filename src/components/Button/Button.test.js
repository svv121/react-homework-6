import Button from './Button';
import { render, screen } from '@testing-library/react';

describe('Button component working', () => {
    test('Should render correctly', () => {
        render(<Button text='Add to cart'/>);
    })

    test('Should Button fragment match snapshot', () =>{
        const { asFragment } = render(<Button text='Add to cart'/>);
        expect(asFragment()).toMatchSnapshot()
    })

    test('Should add text', () => {
        const text = 'Add to cart';
        render(<Button text={text} />);
        expect(screen.getByText(text)).toBeInTheDocument();
    })

    test('Should add class', () => {
        const className = 'buttonsMain';
        render(<Button className={className} text='Add to cart'/>);
        const button = screen.queryByRole('button');
        expect(button.className).toEqual(className);
    })
})