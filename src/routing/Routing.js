import React from "react";
import { Navigate, Routes ,Route } from "react-router-dom";
import Cart from "../pages/Cart/Cart";
import Favorites from "../pages/Favorites/Favorites";
import ProductsAll from "../pages/ProductsAll/ProductsAll";

const Routing = (props) => {
    const {products, incrementCartCount, incrementFavoriteCount} = props
    return (
        <Routes>
            <Route path="/cart"
                   element={<Cart products={products}
                                  incrementCartCount={incrementCartCount} />} />
            <Route path="/favorites"
                   element={<Favorites products={products}
                                       incrementFavoriteCount={incrementFavoriteCount}
                                       incrementCartCount={incrementCartCount} />} />
            <Route path="/"
                   element={<ProductsAll products={products}
                   incrementCartCount={incrementCartCount}
                   incrementFavoriteCount={incrementFavoriteCount} />} />
            <Route path="/home" element={<Navigate replace to="/" />} />
        </Routes>
    );
};

export default Routing;