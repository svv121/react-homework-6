import React from 'react'
import './BuyerForm.scss';

function TextError (props) {
    return <div className='error'>{props.children}</div>
}

export default TextError