import React, { useContext } from 'react';
import ViewContext from '../../contexts/ViewContext/ViewContext';

const ViewSelect= () => {
    const {currentView, setCurrentView} = useContext(ViewContext);

    return (
        <div>
            <select
                name="view"
                id="view"
                value={currentView}
                onChange={({ target: { value } }) => setCurrentView(value)}
            >
                <option value='Table'>Table</option>
                <option value='List'>List</option>
            </select>
        </div>
    );
};

export default ViewSelect;