import { GET_PRODUCTS, CLEAR_CART_IN_LS } from './actionsProducts'

const getProductsAC = () => async (dispatch) => {
    const productsDataInLocalStorage = localStorage.getItem("productsData");
    const productsData = JSON.parse(productsDataInLocalStorage);
    if (productsData) {
        dispatch({ type: GET_PRODUCTS, payload: productsData });
    } else {
        const productsData = await fetch("./products/products.json").then(res => res.json());
        localStorage.setItem("productsData", JSON.stringify(productsData));
        dispatch({ type: GET_PRODUCTS, payload: productsData });
    }
};

const clearCartLsAC = () => dispatch => {
    localStorage.removeItem('cartArr');
    localStorage.removeItem('cartObj');
    localStorage.removeItem('cartCount');
    dispatch({ type: CLEAR_CART_IN_LS });
};

export { getProductsAC, clearCartLsAC }