import Modal from './Modal';
import { render, screen } from '@testing-library/react';

describe('Modal component working', () => {
    test('Should render correctly', () => {
        render(<Modal
            header='testHeader'
            text='Test text in the component Modal'
            LeftBtnTxt='LeftBtnTxt'
            RightBtnTxt='RightBtnTxt'
        />)
    })

    test('Should Modal fragment match snapshot when openCartModal true', () =>{
        const { asFragment } = render(<Modal
            header='testHeader'
            text='Test text in the component Modal'
            LeftBtnTxt='LeftBtnTxt'
            RightBtnTxt='RightBtnTxt'
            openCartModal={true}
        />);
        expect(asFragment()).toMatchSnapshot()
    })

    test('Should Modal fragment match snapshot when openCartModal false', () =>{
        const { asFragment } = render(<Modal
            header='testHeader'
            text='Test text in the component Modal'
            LeftBtnTxt='LeftBtnTxt'
            RightBtnTxt='RightBtnTxt'
            openCartModal={false}
        />);
        expect(asFragment()).toMatchSnapshot()
    })

    test('Should add text', () => {
        const text = 'Test text in the component Modal';
        render(<Modal text={text}
                      header='testHeader'
                      LeftBtnTxt='LeftBtnTxt'
                      RightBtnTxt='RightBtnTxt'
                />)
        expect(screen.getByText(text)).toBeInTheDocument()
    })
})