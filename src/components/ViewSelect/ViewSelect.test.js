import ViewSelect from './ViewSelect';
import { render } from '@testing-library/react';

describe('ViewSelect component working', () => {
    test('Should render correctly', () => {
        render(<ViewSelect
            name="view"
            id="view"
            value="value"
            onChange={() => {}}
        />);
    })

    test('Should ViewSelect fragment match snapshot', () =>{
        const { asFragment } = render(<ViewSelect
                                name="view"
                                id="view"
                                value="value"
                                onChange={() => {}}
                                />);
        expect(asFragment()).toMatchSnapshot()
    })
})