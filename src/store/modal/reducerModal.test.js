import reducerModal from "./reducerModal";
import { IS_OPEN_MODAL } from "./actionsModal";

const initialState = {isOpenModal: false};

describe('Modal reducer working', () => {

    test('Should return the initial state', () => {
        expect(reducerModal(undefined, { type: undefined })).toEqual(initialState)
    })

    test('Should change isOpenModal', () => {
        expect(reducerModal(initialState, { type: IS_OPEN_MODAL, payload: true })).toEqual({
            isOpenModal: true
        })
    })
})