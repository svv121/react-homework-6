import { createContext } from "react";

const ViewContext = createContext('Table');

export default ViewContext;