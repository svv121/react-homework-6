import { useState } from 'react';
import ViewContext from './ViewContext';

const ViewContextProvider = ({children}) => {

	const [currentView, setCurrentView] = useState('Table');

	return (
		<ViewContext.Provider value={{ currentView, setCurrentView }}>
			{children}
		</ViewContext.Provider>
	);
}

export default ViewContextProvider;